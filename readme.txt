=== EDD Download Images ===
Contributors: sticchio,tropicalista
Tags: easy digital downloads, digital downloads, e-downloads, edd, images slider, additional images slider, download images slider
Requires at least: 3.3
Tested up to: 4.4.2
Stable tag: 1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Easily add extra download images and display them.

== Description ==

This plugin requires [Easy Digital Downloads](http://wordpress.org/extend/plugins/easy-digital-downloads/ "Easy Digital Downloads") v2.5 or higher. This plugin allows you to add extra images to your downloads. Use the included shortcode or template tag to display the images slider on your website.

= Shortcode Usage =

    [edd_download_slider]

= Template Tag Usage =

    if( function_exists( 'edd_di_display_slider') ) {
        edd_di_display_slider();
    }


**Plugins for Easy Digital Downloads**

[https://easydigitaldownloads.com/extensions/](https://easydigitaldownloads.com/extensions/ "Plugins for Easy Digital Downloads")

== Installation ==

1. Upload entire `edd-download-slider` to the `/wp-content/plugins/` directory, or just upload the ZIP package via 'Plugins > Add New > Upload' in your WP Admin
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Add additional images to each download


== Screenshots ==

1. The new repeatable image upload fields integrated seamlessly with Easy Digital Downloads

== Upgrade Notice ==


== Changelog ==

= 1.0 =
* Initial release
